# Questions

## Les données persistent-elles lorsqu'on supprime puis recrée un conteneur ?

Non, en général, les données à l'intérieur d'un conteneur ne persistent pas lorsque le conteneur est supprimé.
Les contenurs sont éphémères et ne persiste pas au-delà de leur durée de vie.

## Qu'est-ce qu'un volume ?

Un volume est un espace de stokage externe permettant de garder des données durables partagée entre plusieurs conteneurs.
Il conserve les données même après la suppression d'un conteneur.

## Qu'est-ce qu'un bind mount, et quelle est la différence avec un volume ?

Un bind mount est comme un lien qui connecte un dossier sur votre ordinateur à l'intérieur d'une boîte virtuelle, facilitant le partage de fichiers entre les deux.
Un bind mount relie directement un dossier du système hôte à un conteneur, tandis qu'un volume est un espace de stockage externe géré par Docker.
